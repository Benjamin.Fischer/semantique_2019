import XCTest

extension TP1Tests {
    static let __allTests = [
        ("testExample", testExample),
    ]
}

#if !os(macOS)
public func __allTests() -> [XCTestCaseEntry] {
    return [
        testCase(TP1Tests.__allTests),
    ]
}
#endif
