import XCTest

import Exercice1Tests

var tests = [XCTestCaseEntry]()
tests += Exercice1Tests.allTests()
XCTMain(tests)