import XCTest

import Exercice4Tests

var tests = [XCTestCaseEntry]()
tests += Exercice4Tests.allTests()
XCTMain(tests)